import tensorflow as tf

class Model():
    def __init__ (self, constant_size):
        self.input_generator = tf.placeholder(tf.float32, [None, 800])
        self.input_discriminator = tf.placeholder(tf.float32, [None, 218, 178, 3])

        def clipGradients (x, loss, vars, value):
            grads = x.compute_gradients(loss, var_list=vars)
            clipped = [(tf.clip_by_value(gradx, -value, value), var) for gradx, var in grads]
            opt = x.apply_gradients(clipped)
            return opt

        def normalisation (x):
            return (x - tf.reduce_min(x))/tf.reduce_max(x)

        def deconvolutions (x):
            with tf.variable_scope("generator", reuse=tf.AUTO_REUSE), tf.name_scope("deconvolutions"):

                x = 2 * (x - 0.5)

                # Deconvolute
                this = tf.layers.conv2d_transpose(x, 64, 5, strides=2)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d_transpose(this, 256, 5, strides=1)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d_transpose(this, 128, 5, strides=1)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d_transpose(this, 128, 5, strides=2)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d_transpose(this, 128, 5, strides=1)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d_transpose(this, 128, 5, strides=1)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d_transpose(this, 3, 5, strides=1)
                this = tf.layers.batch_normalization(this)
                this = tf.nn.tanh(this)

                resize = tf.image.resize_images(this, [55, 45])

            return resize

        def convolutions (x):
            with tf.variable_scope("discriminator", reuse=tf.AUTO_REUSE), tf.name_scope("convolutions"):
                this = tf.layers.batch_normalization(x)

                this = tf.layers.conv2d(this, 64, 5, strides=1)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d(this, 64, 5, strides=1)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d(this, 128, 5, strides=2)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d(this, 128, 5, strides=1)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d(this, 128, 5, strides=1)
                this = tf.nn.leaky_relu(this)

                this = tf.layers.conv2d(this, 64, 5, strides=1)
                this = tf.nn.leaky_relu(this)

            return this

        def feedforward (x):
            with tf.variable_scope("discriminator", reuse=tf.AUTO_REUSE), tf.name_scope("feedforward"):
                flatten = tf.layers.flatten(x)
                dense = tf.layers.dense(flatten, 1024, activation=tf.nn.leaky_relu)

                logits = tf.layers.dense(dense, 1)

            return logits

        def discriminator (x):
            return feedforward(convolutions(x))

        def cross_entropy(y, x):
            return tf.reduce_mean(-tf.reduce_sum(y * tf.log(x), 1))

        # reshape
        reshaped_input_generator = tf.reshape(self.input_generator, [-1, 4, 4, 50])
        self.resized_input_discriminator = tf.image.resize_images(self.input_discriminator, [55, 45])
        normalised_input_discriminator = tf.scalar_mul(2/255, self.resized_input_discriminator) - 1

        # generator output
        output_generator = deconvolutions(reshaped_input_generator)
        self.output_generator = tf.cast(tf.scalar_mul(255/2, output_generator) + 255/2, tf.uint8)
        self.output_generator_raw = tf.scalar_mul(255/2, output_generator) + 255/2

        self.logits_fake = discriminator(output_generator)
        self.logits_real = discriminator(normalised_input_discriminator)

        # losses
        lambd = 10
        epsilon = tf.random_uniform([constant_size, 1], minval=0.0, maxval=1.0)
        x_h = epsilon * normalised_input_discriminator + (1 - epsilon) * output_generator

        with tf.variable_scope("", reuse=True):
            grad_d_x_h = tf.gradients(discriminator(x_h), x_h)

        grad_norm = tf.norm(grad_d_x_h[0], axis=1, ord='euclidean')
        grad_pen = tf.reduce_mean(tf.square(grad_norm-1))

        self.loss_discriminator = -tf.reduce_mean(self.logits_real) + tf.reduce_mean(self.logits_fake) + lambd * grad_pen
        self.loss_generator = -tf.reduce_mean(self.logits_fake)

        self.loss_discriminator_display = tf.tanh(tf.abs(self.loss_discriminator))
        self.loss_generator_display = tf.tanh(tf.abs(self.loss_generator))

        # variables
        gen_variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="generator")
        discrim_variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="discriminator")

        # optimisers
        self.optimiser_discriminator = clipGradients(tf.train.RMSPropOptimizer(learning_rate=0.00015, decay=0.99), self.loss_discriminator, discrim_variables, 5)
        self.optimiser_generator = clipGradients(tf.train.RMSPropOptimizer(learning_rate=0.0015, decay=0.99), self.loss_generator, gen_variables, 5)
